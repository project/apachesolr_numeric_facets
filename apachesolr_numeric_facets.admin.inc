<?php

/**
 * @file
 *   Administrative pages for the Apache Solr Numeric Facets.
 */

/**
 * Implementation of hook_admin_settings() for configuring the module
 */
function apachesolr_numeric_facets_admin_settings(&$form_state) {
  $form['apachesolr_numeric_facets']['help'] = array(
    '#type' => 'item',
    '#value' => t('Use this screen to select which numeric/computed fields should be handled using Apache Solr Custom Fields'),
  );

  $field_instances = content_fields();
  foreach ($field_instances as $instance) {
    $field_type = $instance['type'];
    $field_name = $instance['field_name'];
    if(($field_type === 'number_decimal') || ($field_type === 'computed')) {
      $form['apachesolr_numeric_facets']["apachesolr_numeric_facets_{$field_name}"] = array(
        '#type' => 'checkbox',
        '#title' => t($field_name),
      '#default_value' => variable_get("apachesolr_numeric_facets_{$field_name}", FALSE),
      );
    }
  }

  $has_facets = (bool)$field_instances;

  $form['no-facets-message'] = array(
    '#value' => t('<em>No numeric/computed fields are available</em>'),
    '#access' => !$has_facets,
  );

  return system_settings_form($form);
}
